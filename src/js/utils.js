export function renderItems ({ wrap, items, countSlide, marginSlide }) {
  const widthWrap = wrap.getBoundingClientRect().width
  const widthItem = ((widthWrap - marginSlide * (countSlide - 1)) / countSlide).toFixed()

  items.forEach((item, index) => {
    const slide = document.createElement('div')
    slide.classList.add('slider__item')
    slide.setAttribute('data-slide-index', index)
    slide.insertAdjacentHTML('beforeend', `
      <div class="desc">
        <span>${ item.desc }</span>
       </div>
    `)
    css(slide, {
      left: `${index * (+widthItem + +marginSlide)}px`,
      width: `${widthItem}px`,
      backgroundImage: `url(${item.img})`
    })
    wrap.insertAdjacentElement('beforeend', slide)
  })
  return wrap
}

export function removeActiveClasses (wrap) {
  const items = wrap.childNodes
  items.forEach(child => child.classList.remove('slider__active'))
}

export function css (el, styles = {}) {
  Object.assign(el.style, styles)
}