import '../src/styles/style.styl'
import { slider } from '../index'

window.instSlider = slider({
  elem: '#slider',
  marginSlide: 5,
  height: 600,
  autoSlide: true,
  speedSlide: 1400,
  loop: true,
  items: [
    {
      img: 'https://cdn.pixabay.com/photo/2012/06/19/10/32/owl-50267_960_720.jpg',
      desc: 'desc'
    },
    {
      img: 'https://cdn.pixabay.com/photo/2020/06/21/09/52/abendstimmung-5324159_960_720.jpg',
      desc: 'desc'
    },
    {
      img: 'https://cdn.pixabay.com/photo/2021/06/12/21/10/sea-6331772_960_720.jpg',
      desc: 'desc'
    },
    {
      img: 'https://cdn.pixabay.com/photo/2021/07/07/13/11/lake-6394315_960_720.jpg',
      desc: 'description'
    },
    {
      img: 'https://cdn.pixabay.com/photo/2021/07/10/07/32/scottish-fold-6400836_960_720.jpg',
      desc: 'desc'
    },
    {
      img: 'https://cdn.pixabay.com/photo/2021/07/09/15/35/biplane-6399408_960_720.jpg',
      desc: 'desc'
    },
    {
      img: 'https://cdn.pixabay.com/photo/2021/07/09/02/43/jokulsarlon-6398116_960_720.jpg',
      desc: 'desc'
    },
    {
      img: 'https://cdn.pixabay.com/photo/2021/07/08/03/55/mount-everest-6395759_960_720.jpg',
      desc: 'desc'
    },
    {
      img: 'https://cdn.pixabay.com/photo/2021/07/08/14/07/mountains-6397006_960_720.jpg',
      desc: 'desc'
    }
  ]
})

instSlider.init()