import {
	renderItems,
	removeActiveClasses,
	css
} from './src/js/utils'

const PERCENT_ACTIVE_ITEM = 60

export function slider(options) {
	const {
		elem,
		items,
		marginSlide = 10,
		height = 600,
		autoSlide = false,
		speedSlide = 500,
		loop = false
	} = options
	const countSlide = items.length
	const $root = document.querySelector(elem)
	let wrap = null
	let intervalSlide = null
	let pauseSlide = false
	let activeSlideIndex = 0

	window.addEventListener('resize', resize)
	$root.addEventListener('click', clickSlide)
	$root.addEventListener('mouseenter', mouseenter)
	$root.addEventListener('mouseleave', mouseleave)

	const render = () => {
		wrap = document.createElement('div')
		wrap.classList.add('slider__items')
		css(wrap, {
			height: `${height}px`
		})
		$root.insertAdjacentElement(
			'afterbegin',
			wrap
		)
		renderItems({
			wrap,
			items,
			countSlide,
			marginSlide
		})
	}

	const checkActiveSlide = () => {
		const items = wrap.childNodes
		const widthWrap = wrap.getBoundingClientRect().width
		const activeItem = items[activeSlideIndex]
		const share = PERCENT_ACTIVE_ITEM / 100
		const widthActiveItem = (widthWrap * share).toFixed()
		const widthItem = ( ( (widthWrap - widthActiveItem) - marginSlide * (countSlide - 1) ) / (countSlide - 1) ).toFixed()

		removeActiveClasses(wrap)

		activeItem.classList.add('slider__active')
		items.forEach((item, i) => {
			const width = i === +activeSlideIndex ? widthActiveItem : widthItem
			const offset = i > +activeSlideIndex ? widthActiveItem - widthItem: 0
			css(item, {
				width: `${width}px`,
				left: `${i * (+widthItem + +marginSlide) + +offset}px`,
			})
		})
	}

	const autoSlideInterval = () => {
		if (!autoSlide) return
		intervalSlide = setInterval(() => {
			if (pauseSlide) return;
			activeSlideIndex++
			if (activeSlideIndex > items.length - 1) {
				if (loop) {
					activeSlideIndex = 0
				} else {
					clearInterval(intervalSlide)
					return
				}
			}
			checkActiveSlide()
		}, speedSlide)
	}

	function clickSlide (e) {
		const elem = e.target.dataset
		if (elem.slideIndex) {
			activeSlideIndex = elem.slideIndex
			checkActiveSlide()
		}
	}

	function mouseenter () {
		pauseSlide = true
	}

	function mouseleave () {
		pauseSlide = false
	}

	function resize () {
		checkActiveSlide()
	}

	return {
		init () {
			if (elem && items.length) {
				render()
				checkActiveSlide()
				autoSlideInterval()
			}
		},
		currSlide (index) {
			activeSlideIndex = index
			checkActiveSlide()
		},
		destroy () {
			window.removeEventListener('resize', resize)
			$root.removeEventListener('click', clickSlide)
			$root.removeEventListener('mouseenter', mouseenter)
			$root.removeEventListener('mouseleave', mouseleave)
			$root.innerText = ''
			clearInterval(intervalSlide)
		}
	}
}
